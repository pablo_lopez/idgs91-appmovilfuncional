package com.lopez.pablo.convertidor;

import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends WearableActivity {

    //private TextView mTextView;
    public double miles,kms;
    public String result, miles_string;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button result_btn=(Button)findViewById(R.id.btn_convert);
        result_btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                convertMiles();
            }
        });
        //mTextView = (TextView) findViewById(R.id.text);

        // Enables Always-on
        setAmbientEnabled();
    }

    public void convertMiles(){
        TextView result_tv=(TextView)findViewById(R.id.tv_km);
        EditText miles_et=(EditText)findViewById(R.id.ed_mile);

        miles_string=miles_et.getEditableText().toString();
        miles=Double.parseDouble(miles_string);

        kms=miles*1.609;
        result=kms+" KM";
        result_tv.setText(result);
    }
}